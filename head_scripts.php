<?php

session_start();
header("content-type: text/javascript");

function getImageSizes($string) {
	$im = imagecreatefromstring(file_get_contents($string));
	return array(imagesx($im),imagesy($im));
}

$air_route_sizes = getImageSizes(dirname(__FILE__)."/".$_SESSION["air_route"]);

?>

var go_bird = function() {
	var bird = document.getElementById("air_route");
	var pos = bird.style.backgroundPosition;
	if (pos.indexOf(" ") < 0)
		pos = parseInt(bird.parentNode.scrollWidth)+<?php echo $air_route_sizes[0]; ?>+"px 0";
	pos = pos.split(" ");
	var x = parseInt(pos[0]);
	var y = parseInt(pos[1]);
	if (x < -<?php echo $air_route_sizes[0]; ?>) {
		x = parseInt(bird.parentNode.scrollWidth) + <?php echo $air_route_sizes[0]; ?>;
		y = 0;
		var top = bird.offsetTop;
		var ally = bird.parentNode.scrollHeight;
		var miny = Math.round(ally/6);
		var maxy = Math.round(ally/3*2);
		top = Math.floor(Math.random()*(maxy-miny))+miny;
		var timeout = (Math.floor(Math.random()*19)+2)*1000;
		bird.style.top = top + "px";
	} else {
		x--;
		var maxy = <?php echo $air_route_sizes[1]; ?>;
		y -= bird.scrollHeight;
		if (Math.abs(y) >= maxy) y = 0;
		var timeout = 40;
	}
	bird.style.backgroundPosition = x+"px "+y+"px";
	window.setTimeout("go_bird()", timeout);
	return false;
}

window.onload = function() {
	go_bird();
}