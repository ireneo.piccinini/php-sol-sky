<?php

session_start();
header("content-type: text/html");

function it_date($date,$uc = true) {
	$source = array (
		"january" => "gennaio",
		"february" => "febbraio",
		"march" => "marzo",
		"april" => "aprile",
		"may" => "maggio",
		"june" => "giugno",
		"july" => "luglio",
		"august" => "agosto",
		"september" => "settembre",
		"october" => "ottobre",
		"novembre" => "november",
		"december" => "dicembre"
	);
	$search = array_keys($source);
	$replace = array_values($source);
	$return = str_ireplace($search,$replace,$date);
	if ($uc) $return = ucwords($return);
	return $return;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<title>Sol Facts</title>
	<link rel="stylesheet" type="text/css" href="head_style.php" />
	<link rel="stylesheet" type="text/css" href="style.php" />
	<script type="text/javascript" src="head_scripts.php"></script>
</head>
<body>
	<div id="sky">
		<div class="sky sky_notte"></div>
		<div class="sky sky_mattino"></div>
		<div class="sky sky_pomeriggio"></div>
		<div class="sky sky_sera"></div>
		<div id="Luna"></div>
		<div id="Sol"></div>
		<div id="air_weather"></div>
		<div id="air_route"></div>
	</div>
	<h1 id="header">
		<a href="/">da.cambiare</a>
	</h1>
	<div id="head_time" class="datetime">
		<p class="date"><?php echo it_date(date("j F")); ?></p>
		<p class="time"><?php echo date("H:i"); ?></p>
	</div>
	<div id="content">
<?php include "html.inc"; ?>
	</div>
</body>
</html>