<?php

session_start();
header("content-type: text/css");
define("MOMENTO", $_SESSION["momento"]);
$BgColors = array(
	"mattino" => '#093',
	"pomeriggio" => '#093',
	"sera" => '#030',
	"notte" => '#000',
);

?>

body {
	margin: 0;
	text-align: center;
	font: 13px Helvetica,Arial,sans-serif;
	background: <?php echo $BgColors[MOMENTO]; ?> url('images/grass/grass_<?php echo MOMENTO; ?>.png');
}

.datetime p {
	margin: 0;
	text-transform: uppercase;
	line-height: 100%;
}

.datetime .time {
	font-weight: bold;
	font-size: 2em;
}

#content {
	margin: 15px auto 0 auto;
	-moz-border-radius-topright: 10px;
	-moz-border-radius-topleft: 10px;
	-webkit-border-top-right-radius: 10px;
	-webkit-border-top-left-radius: 10px;
	border-style: solid;
	border-width: 1px 1px 0 1px;
	border-color: #060;
	padding: 10px;
	width: 800px;
	background: #fff;
}