<?php

session_start();
header("content-type: text/css");

include("calculate.php");
$mom_sfondi = array(
	"mattino" => '0ff',
	"pomeriggio" => '09f',
	"sera" => 'c06',
	"notte" => '003',
	);
$_SESSION["momento"] = MOMENTO;
define("SKY_HEIGHT", 300);
$_SESSION["air_route"] = "images/sky/air_".MOMENTO.".png";
$boolean = ($sera > $pomeriggio || $notte > $mattino) ? "night" : "day";
$weather_file = file(dirname(__FILE__)."/images/sky/weather/bgs.txt",FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
$weather_css = array();
$weather_cur = 0;
foreach ($weather_file as $line) {
	if (preg_match('/^[A-Z]/',$line)) {
		$weather_cur = trim(strtolower($line));
		$weather_css[$weather_cur] = array();
	} else
		$weather_css[$weather_cur][] = trim($line);
}
foreach ($weather_css as $weather_class => $array) if(in_array(CODENAME,$array)) break;
if (!in_array(CODENAME,$array)) $weather_class = "none";
unset($weather_file,$weather_cur,$line,$array,$weather_css);

function calculate_bgcolor() {
	switch(MOMENTO){
		case "notte":
			return "000";
		case "mattino": case "sera":
			return "406080";
		default:
			return "fff";
	}
}

?>

#sky {
	position: relative;
	width: 100%;
	height: <?php echo SKY_HEIGHT; ?>px;
	background-color: #<?php echo calculate_bgcolor(); ?>;
	overflow: hidden;
	z-index: -1;
}

#sky .sky {
	position: absolute;
	top: 0; left: 0;
	width: 100%;
	height: 100%;
}

<?php
foreach($momenti as $momento)
	echo "
.sky_{$momento} {
	background: #{$mom_sfondi[$momento]} url('images/sky/sky_{$momento}.png') repeat-x 0 0;
".($$momento?
"	opacity: ".($$momento/100).";
	filter: alpha(opacity=".$$momento.");":
"	display: none;")."
}";
?>

h1#header {
	position: absolute;
	top: 0; left: 0;
	margin: 0;
	width: 100%;
	height: <?php echo SKY_HEIGHT; ?>px;
	background: transparent url('images/header/grass/grass_<?php echo MOMENTO; ?>.png') repeat-x left bottom;
	text-shadow: #000 2px 2px 4px;
}

h1#header a {
	display: block;
	margin: <?php echo round(SKY_HEIGHT/2.5); ?>px auto 0 auto;
	width: 500px;
	color: #fff;
	text-decoration: none;
	letter-spacing: 1px;
}

#head_time {
	position: absolute;
	top: 10px; right: 10px;
	color: #fff;
	text-align: right;
}

#Luna {
<?php if(is_array($Luna)) { ?>
	position: absolute;
	top: <?php echo 100 - LUNA_DECL + $Luna["y"]; ?>%;
	right: <?php echo $Luna["x"]; ?>%;
	margin-top: -50px;
	width: 100px; height: 100px;
	background: transparent url('images/sky/moon/<?php echo LUNA_FASE."-".$boolean; ?>.png') no-repeat center center;
	opacity: <?php echo LUNA_OP/100; ?>;
	filter: alpha(opacity=<?php echo LUNA_OP; ?>);
<?php } else { ?>
	display: none;
<?php } ?>
}

#Sol {
<?php if($Sol >= 0) { ?>
	position: absolute;
	top: <?php echo $Sol; ?>%;
	left: 20%;
	margin-top: -45px;
	width: 90px; height: 90px;
	background: transparent url('images/sky/Sol.png') no-repeat center center;
<?php } else { ?>
	display: none;
<?php } ?>
}

#air_weather {
	position: absolute;
	top: 0; left: 0;
	width: 100%;
	height: 100%;
	background: transparent url('images/sky/weather/<?php echo $weather_class."_".$boolean; ?>.png') repeat-x 0 0;
	opacity: .75;
	filter: alpha(opacity=75);
}

#air_route {
	position: absolute;
	top: <?php echo rand(round(SKY_HEIGHT/6),round(SKY_HEIGHT/3*2)); ?>px;
	left: 0;
	width: 100%;
	height: 20px;
	background: transparent url('<?php echo $_SESSION["air_route"]; ?>') no-repeat -9999px 0;
	opacity: .6;
	filter: alpha(opacity=60);
}