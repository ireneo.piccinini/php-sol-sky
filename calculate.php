<?php

function debug() {
	$constants = get_defined_constants(true);
	$globals = print_r($GLOBALS,true);
	echo "<pre>".print_r($constants["user"],true).$globals."</pre>";
}
function normalize($v) {
	$v = $v - floor($v);
	if($v < 0) {
		$v = $v + 1;
	}
	return $v;
}
function getdata($name = 0,$alt = 0,$match = 0,$dir = "stored_data") {
	if(!$name) return;
	if(!is_dir($dir)) {
		mkdir($dir);
		return;
	}
	$now = date("Ymd");
	$filename = "$dir/$name"."_$now";
	if(!is_file($filename)) {
		if(!($data = file_get_contents($alt))) return;
		foreach(scandir($dir) as $ls)
			if(is_file("$dir/$ls")&&strpos($ls,$name."_")!==false)
				unlink("$dir/$ls");
		if($match && preg_match($match,$data,$matches))
			$data = $matches[0];
		$file = fopen($filename,'w');
		fwrite($file,$data);
		fclose($file);
	} else
		$data = file_get_contents($filename);
	return $data;
}

# Informazioni di localizzazione spazio-temporale
define("LAT", 44.55);
define("LONG", 11.29);
define("LONG0", 15);
define("GIORNO_ANNO", date("z")+1);

# Definizione di costanti utili e di orari chiave
$eq_tempo = 7.367*cos(0.01720279*GIORNO_ANNO+1.4981)+9.9182*cos(0.03440558*GIORNO_ANNO+1.9196)+0.306*cos(0.05160838*GIORNO_ANNO+1.8089)+0.2027*cos(0.06881117*GIORNO_ANNO+2.2459);
define("EQ_TEMPO", $eq_tempo);

define("COST_LOCALE", LONG - LONG0);
$declinazione_sole = 0.3838+23.2623*cos(0.01720279*GIORNO_ANNO-2.965)+0.3552*cos(0.03440558*GIORNO_ANNO-3.0635)+0.1342*cos(0.05160838*GIORNO_ANNO-2.5897)+0.0326*cos(0.06881117*GIORNO_ANNO+0.0511);
define("DECLINAZIONE_SOLE", $declinazione_sole);

define("MEZZOGIORNO", round(12 - EQ_TEMPO / 60 - COST_LOCALE / 15, 2));
$angolo_orario_delta2 = acos((-0.01454-sin(LAT*M_PI/180)*sin(DECLINAZIONE_SOLE*M_PI/180))/(cos(LAT*M_PI/180)*cos(DECLINAZIONE_SOLE*M_PI/180)))*180/M_PI;
define("ANGOLO_ORARIO_DELTA2", $angolo_orario_delta2);

define("ALBA", round(MEZZOGIORNO - ANGOLO_ORARIO_DELTA2 / 15, 2));
define("TRAMONTO", round(MEZZOGIORNO + ANGOLO_ORARIO_DELTA2 / 15, 2));
define("MEZZANOTTE", MEZZOGIORNO+12);
define("MEZZANOTTE2", MEZZOGIORNO-12);

$delta3_crepuscolo = acos((sin(-18*M_PI/180)-(sin(LAT*M_PI/180)*sin(DECLINAZIONE_SOLE*M_PI/180)))/((cos(LAT*M_PI/180)*cos(DECLINAZIONE_SOLE*M_PI/180))))*180/M_PI;

define("DELTA3_CREPUSCOLO", $delta3_crepuscolo);
define("DURATA_CREPUSCOLO", abs(DELTA3_CREPUSCOLO-ANGOLO_ORARIO_DELTA2)/15);

define("INIZIO_CREPUSCOLO", round(ALBA-DURATA_CREPUSCOLO,2));
define("FINE_CREPUSCOLO", round(TRAMONTO+DURATA_CREPUSCOLO,2));

# Ora corrente, in decimi
define("NOW", date("G")+round(date("i")/60,2));

# Fase del giorno corrente
$momenti = array("mattino","pomeriggio","sera","notte");
foreach($momenti as $momento) $$momento = 0;
if(NOW >= INIZIO_CREPUSCOLO && NOW < ALBA):			# Aurora
	$mattino = round((NOW-INIZIO_CREPUSCOLO)*100/(ALBA-INIZIO_CREPUSCOLO));
	$notte = 100 - $mattino;
	$momento = ($mattino > $notte) ? "mattino" : "notte";
elseif(NOW >= ALBA && NOW < MEZZOGIORNO):			# Mattino
	$pomeriggio = round((NOW-ALBA)*100/(MEZZOGIORNO-ALBA));
	$mattino = 100 - $pomeriggio;
	$momento = ($pomeriggio > $mattino) ? "pomeriggio" : "mattino";
elseif(NOW >= MEZZOGIORNO && NOW < TRAMONTO):		# Pomeriggio
	$pomeriggio = 100;
	$momento = "pomeriggio";
elseif(NOW >= TRAMONTO && NOW < FINE_CREPUSCOLO):	# Crepuscolo
	$sera =  round((NOW-TRAMONTO)*100/(FINE_CREPUSCOLO-TRAMONTO));
	$pomeriggio = 100 - $sera;
	$momento = ($sera > $pomeriggio) ? "sera" : "pomeriggio";
elseif(NOW >= FINE_CREPUSCOLO && NOW < MEZZANOTTE):	# Sera
	$notte = round((NOW-MEZZANOTTE2)*100/(ALBA-MEZZANOTTE2));
	$sera = 100 - $notte;
	$momento = ($notte > $sera) ? "notte" : "sera";
else:												# Notte
	$notte =  100;
	$momento = "notte";
endif;
define("MOMENTO", $momento);
unset($eq_tempo,$declinazione_sole,$angolo_orario_delta2,$delta3_crepuscolo,$momento);

# Posizione di Sol
$Sol = -1;
if (NOW >= MEZZOGIORNO && NOW <= FINE_CREPUSCOLO) {
	if (NOW < TRAMONTO) $Sol = round((NOW-MEZZOGIORNO)/(TRAMONTO-MEZZOGIORNO)*100);
	else $Sol = 100 + round((NOW-TRAMONTO)/(FINE_CREPUSCOLO-TRAMONTO)*100);
}

# Sincronizzazione con il meteo (Yahoo)
$yahoo_feed = "http://weather.yahooapis.com/forecastrss?w=711080&u=c";
$xml = ($temp = file_get_contents($yahoo_feed)) ? $temp : 0;
$code = 3200; $temp = 20;
if (preg_match('/^<[^:]+:condition.+code="(?P<code>\d+)".+temp="(?P<temp>\d+)".+$/m',$xml,$matches)) {
	$code = $matches["code"];
	$temp = $matches["temp"];
}
include_once("yahoo-weather-codes.php");
$codename = preg_replace('/\s+/','_',$yahoo_weathers[$code]);
$codename = preg_replace('/\([^)]+\)|_(?=\()|(?<=\))_|\W/','',$codename);
define("WEATHERCODE", $code);
define("CODENAME", $codename);
define("TEMPERATURE", $temp);
unset($yahoo_feed,$xml,$temp,$codename,$code);

# Opacit� della Luna
$Luna = 100;
if(NOW > INIZIO_CREPUSCOLO && NOW < FINE_CREPUSCOLO){
	$frag = MEZZOGIORNO - INIZIO_CREPUSCOLO;
	if(NOW <= MEZZOGIORNO) $Luna = ($frag-NOW+INIZIO_CREPUSCOLO)/$frag*70;
	else $Luna = (NOW-MEZZOGIORNO)/$frag*70;
	$Luna += 5;
}
define("LUNA_OP", round($Luna));
unset ($frag,$Luna);

# Fase della Luna
function moon_phase() {
	$y = date('Y');
	$m = date('n');
	$d = date('j');
	$yy = $y - floor((12 - $m) / 10);
	$mm = $m + 9;
	if($mm >= 12) $mm -= 12;
	$k1 = floor(365.25 * ($yy + 4712));
	$k2 = floor(30.6 * $mm + 0.5);
	$k3 = floor(floor(($yy / 100) + 49) * 0.75) - 38;
	$jd = $k1 + $k2 + $d + 59;
	if($jd > 2299160) $jd -= $k3;
	$ip = normalize(($jd - 2451550.1) / 29.530588853);
	$ag = $ip * 29.53;
    if($ag >= 1.84566 && $ag < 5.53699)
    	$phase = "crescente";
    elseif($ag < 9.22831)
		$phase = "primo-quarto";
	elseif($ag < 12.91963)
		$phase = "gibbosa-crescente";
	elseif($ag < 16.61096)
		$phase = "piena";
	elseif($ag < 20.30228)
		$phase = "gibbosa-calante";
	elseif($ag < 23.99361)
		$phase = "ultimo-quarto";
	elseif($ag < 27.68493)
		$phase = "calante";
	else
		$phase = "nuova";
	return $phase;
}
define("LUNA_FASE", moon_phase());

# Effemeridi e posizione della Luna
define("LUNA_DECL", round((90-(LAT-28.45))/9*10));
$luna = array(14,150,3,150);
$feedmerids = "http://www.nautica.it/go.php?page=ephem&site=N&location=BO";
$regex = '@<b[^>]*>la luna</b>.*?<b[^>]*>(\d+:\d+)</b>.*?<b[^>]*>([\d.]{1,6})�</b>.*?<b[^>]*>(\d+:\d+)</b>.*?<b[^>]*>([\d.]{1,6})�</b>@mi';
$feedmerids = getdata("feedmerids",$feedmerids,preg_replace('/[()]/','',$regex));
if(preg_match($regex,$feedmerids,$m)) $luna = array(explode(':',$m[1]),$m[2],explode(':',$m[3]),$m[4]);
# Normalizzazione valori
$luna[0] = round($luna[0][0]+$luna[0][1]/60,2);
$luna[1] = round($luna[1]);
$luna[2] = round($luna[2][0]+$luna[2][1]/60,2);
$luna[3] = round($luna[3]);
# Posizione verticale
$now = ($luna[2]<$luna[0] && NOW<$luna[0]) ? NOW+24 : NOW;
if($luna[2] < $luna[0]) $luna[2] += 24;
$mezzaluna = ($luna[2]-$luna[0])/2+$luna[0];
$Luna["y"] = -1;
if($now>$luna[0] && $now<$mezzaluna)
	$Luna["y"] = round(($now-$luna[0])/($mezzaluna-$luna[0])*LUNA_DECL);
elseif($now>$mezzaluna && $now<$luna[1])
	$Luna["y"] = LUNA_DECL - round(($now-$mezzaluna)/($luna[2]-$mezzaluna)*LUNA_DECL);
# Posizione orizzontale
$Luna["x"] = -1;
if($Luna["y"] > -1){
	if($luna[3] < $luna[1]) $luna[3] += 360;
	$luna[9] = ($now-$luna[0])/($luna[2]-$luna[0])*($luna[3]-$luna[1])+$luna[1];
	if($luna[9] > 45 && $luna[9] < 135)
		round($Luna["x"] = round(($luna[9]-$luna[1])/($luna[3]-$luna[1])*100));
}
if($Luna["x"] < 0 || $Luna["y"] < 0) $Luna = 0;
unset($luna,$feedmerids,$regex,$mezzaluna);

//debug();

?>